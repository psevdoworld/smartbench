def print_format_table():
    """
    prints table of formatted text format options
    """
    for style in range(8):
        for fg in range(30,38):
            s1 = ''
            for bg in range(40,48):
                format = ';'.join([str(style), str(fg), str(bg)])
                s1 += '\x1b[%sm %s \x1b[0m' % (format, format)
            print(s1)
        print('\n')

print_format_table()

RESET = '\033[0m'
def get_color_escape(r, g, b, background=False):
    return '\033[{};5;{};{};{}m'.format(48 if background else 36, r, g, b)
print(get_color_escape(250,1,12)
          + get_color_escape(0,0,251, True)
          + '▀' + RESET)
