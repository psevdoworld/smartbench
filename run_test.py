from pybluez.bluetooth import *


draw_in_console=True

def main():
    server_sock=BluetoothSocket( RFCOMM )
    server_sock.bind(("",PORT_ANY))
    server_sock.listen(1)

    port = server_sock.getsockname()[1]

    uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

    advertise_service( server_sock, "SampleServer",
                       service_id = uuid,
                       service_classes = [ uuid, SERIAL_PORT_CLASS ],
                       profiles = [ SERIAL_PORT_PROFILE ],
    #                   protocols = [ OBEX_UUID ]
                        )

    print("Waiting for connection on RFCOMM channel %d" % port)

    client_sock, client_info = server_sock.accept()
    print("Accepted connection from ", client_info)
    display=Display()
    try:
        while True:
            data = client_sock.recv(1024)
            if len(data) == 0: break
            display.process_commands(data)
    except IOError:
        pass

    print("disconnected")

    client_sock.close()
    server_sock.close()
    print("all done")

class Display:
    n=20
    m=54

    def __init__(self):
        self._display=display=[["0x000000" for i in range(self.m)] for j in range(self.n)]
    def __str__(self):
        RESET = '\033[0m'
        def get_color_escape(r, g, b, background=False):
            return '\033[{};2;{};{};{}m'.format(48 if background else 38, r, g, b)
        disp_str=''
        for x in range(self.n):
            for y in range(self.m):
                rgb=[int(self._display[x][y][i:i+2], 16) for i in (2, 4, 6)]
                disp_str+=('\033[36;5;'+str(rgb[0])+';'+str(rgb[1])+';'+str(rgb[2])+'m'+"▮"+'\033[0m')
            disp_str+="\n"
        return(disp_str)

    def draw(self,color,x,y):
        self._display[x][y]=color
        if draw_in_console:
            print(self._display)
            print(self)
    def process_commands(self,data):
        data=data.decode("utf-8")[:-2]
        for command in data.split("\r\n"):
            if command=="device_connected":
                pass
            elif command=="mode_draw":
                pass
            elif command[:5]=="draw ":
                color=command[5:13]
                coords=command[14:-1]
                y,x=map(int, coords.split(":"))
                self.draw(color,x,y)

main()
